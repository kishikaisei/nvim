local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
	return
end

local dashboard = require("alpha.themes.dashboard")
dashboard.section.header.val = require('user.config.alpha_art')
dashboard.section.buttons.val = {
	dashboard.button("e", "  New file", ":ene <BAR> startinsert<CR>"),
	dashboard.button("p", "  Find project", ":Telescope projects<CR>"),
	dashboard.button("s", "  Find Session", ":Autosession search<CR>"),
	dashboard.button("h", "  Recently used files", ":Telescope oldfiles<CR>"),
	dashboard.button("c", "  Configuration", ":e $MYVIMRC<CR>"),
	dashboard.button("q", "  Quit Neovim", ":qa!<CR>"),
}

local function footer()
  -- NOTE: requires the fortune-mod package to work
	-- local handle = io.popen("quote")
	-- local fortune = handle:read("*a")
	-- handle:close()
	-- return fortune
	return "neovim.com"
end

dashboard.section.footer.val = footer()

dashboard.section.footer.opts.hl = "Type"
dashboard.section.header.opts.hl = "Include"
dashboard.section.buttons.opts.hl = "Keyword"

dashboard.opts.opts.noautocmd = true
alpha.setup(dashboard.opts)

