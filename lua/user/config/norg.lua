local status_ok, neorg = pcall(require, "neorg")
if not status_ok then
	return
end

local home = os.getenv("HOME")
neorg.setup {
	-- Tell Neorg what modules to load
	load = {
		["core.defaults"] = {}, -- Load all the default modules
		["core.keybinds"] = { -- Configure core.keybinds
			config = {
				default_keybinds = true, -- Generate the default keybinds
				neorg_leader = "<Leader>o" -- This is the default if unspecified
			}
		},
		["core.norg.concealer"] = {}, -- Allows for use of icons
		["core.norg.dirman"] = { -- Manage your directories with Neorg
			config = {
				workspaces = {
          my_workspace = home .. [[\Documents\norg\]]
				}
			}
		},
    ["core.norg.completion"] = {
      config = {
        engine = "nvim-cmp"
      }
    }
	},
}

-- vim.cmd[[:autocmd FileType norg :normal ggzc]]

