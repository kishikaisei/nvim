local g = vim.g

g.mundo_preview_bottom  = 1
g.mundo_width           = 35
g.mundo_preview_height  = 30
g.mundo_right           = 1
--g.mundo_close_on_revert = 1
