local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

---- Normal ----
-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Intuative navigation in wrapped lines
keymap('n', 'k', 'gk', opts)
keymap('n', 'j', 'gj', opts)
keymap('n', '0', '^', opts)
keymap('n', '^', '0', opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Stay in indent mode
keymap('n', '<', '<s-v><g<Esc>', opts)
keymap('n', '>', '<s-v>>g<Esc>', opts)

-- Next and Previous with centering
keymap('n', 'n', 'nzzzv', opts)
keymap('n', 'N', 'Nzzzv', opts)

-- Navigate buffers
keymap("n", "<S-l>",  ":BufferLineCycleNext<CR>", opts)
keymap("n", "<S-h>",  ":BufferLineCyclePrev<CR>", opts)
-- keymap("n", "<C-A-l>", ":BufferLineMoveNext<CR>", opts)
-- keymap("n", "<C-A-h>", ":BufferLineMovePrev<CR>", opts)

-- Searching clears highlights
keymap('n', '/', ':noh<CR>/', opts)

-- No yanking when deleting/changing
keymap('n', 'd', '"_d', opts)
keymap('n', '<s-d>', '"_D', opts)
keymap('n', 'c', '"_c', opts)
keymap('n', '<s-c>', '"_C', opts)
keymap('n', 'x', '"_x', opts)


---- Insert ----
-- Ctrl + v to paste in insert mode
keymap('i', '<C-v>', '<C-R>*', opts)

---- Visual ----
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

keymap("v", "p", '"_dP', opts)

-- No yanking when deleting/changing
keymap('v', 'd', '"_d', opts)
keymap('v', 'c', '"_c', opts)
keymap('v', 'x', '"_x', opts)

---- Terminal ----
-- Esc to exit terminal mode
keymap('t', '<Esc>', '<C-\\><C-n>', opts)

-- Better terminal navigation
-- keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
-- keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
-- keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
-- keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

---- Command ----
keymap('c', '<C-v>', '<C-R>* ', opts)

vim.cmd([[command WQ wq]])
vim.cmd([[command Wq wq]])
vim.cmd([[command W w]])
vim.cmd([[command Q q]])
