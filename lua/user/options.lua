local options = {
  fileencoding = "utf-8",                  -- The encoding written to a file
  autochdir = true,                        -- Automatically CD to file folder
  lazyredraw = true,                       -- Better scrolling performance on large results

  guifont = "FiraCode Nerd Font Mono:h10", -- The font used in graphical neovim applications
  background = "dark",                     -- Set background to be dark
  clipboard = "unnamedplus",               -- Allows neovim to access the system clipboard
  completeopt = { "menuone", "noselect" }, -- Mostly just for cmp
  hlsearch = true,                         -- Highlight all matches on previous search pattern

  showmode = false,                        -- We don't need to see things like -- INSERT -- anymore
  cmdheight = 1,                           -- More space in the neovim command line for displaying messages
  conceallevel = 0,                        -- So that `` is visible in markdown files
  signcolumn = "yes",                      -- Always show the sign column, otherwise it would shift the text each time
  virtualedit = 'block',                   -- In visual, can select beyond EOL

  cursorline = true,                       -- Highlight the current line
  wrap = false,                            -- Display lines as one long line
  number = true,                           -- Set numbered lines
  relativenumber = false,                  -- Set relative numbered lines

  list = true,                             -- Enable showing invisble charachters
  -- listchars = 'tab: ,trail:·,space:·,eol:,nbsp:⣿', -- Show invisible characters with the specified symbnls

  numberwidth = 4,                         -- Set number column width to 2 {default 4}
  showtabline = 2,                         -- Always show tabs
  splitbelow = true,                       -- Force all horizontal splits to go below current window
  splitright = true,                       -- Force all vertical splits to go to the right of current window
  mouse = "a",                             -- Allow the mouse to be used in neovim
  pumheight = 10,                          -- Pop up menu height
  ignorecase = true,                       -- Ignore case in search patterns
  smartcase = true,                        -- Smart case
  smartindent = true,                      -- Make indenting smarter again
  termguicolors = true,                    -- Set term gui colors (most terminals support this)
  timeoutlen = 100,                        -- Time to wait for a mapped sequence to complete (in milliseconds)
  updatetime = 300,                        -- Faster completion (4000ms default)
  writebackup = false,                     -- If a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
  expandtab = true,                        -- Convert tabs to spaces
  shiftwidth = 2,                          -- The number of spaces inserted for each indentation
  tabstop = 2,                             -- Insert 2 spaces for a tab
  scrolloff = 8,                           -- Overhead vertically
  sidescrolloff = 8,                       -- Overhead horizontally

  backup = false,                          -- Creates a backup file
  -- backupdir = '',
  swapfile = false,                        -- Creates a swapfile
  -- set.directory = '',
  undofile = true,                         -- Enable persistent undo
  -- set.undodir = os.getenv("NVIMRC") .. [[\tmp\undotree\]],
}

vim.opt.shortmess:append "c"

for k, v in pairs(options) do
  vim.opt[k] = v
end

vim.opt.wildignore = { '*.dll', '*.exe', '*.p3d' }

vim.cmd [[
try
  colorscheme onenord
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry
]]

vim.cmd [[set whichwrap+=<,>,[,],h,l]]
vim.cmd [[set iskeyword+=-]]
vim.cmd [[set formatoptions-=cro]] -- TODO: this doesn't seem to work

if jit.os == 'Windows' then
  vim.g.python3_host_prog = [[C:\Users\neil.messelmani\scoop\apps\python\current\python.exe]]		-- Python path
end

-- Set filetype to specific
vim.cmd("au BufRead,BufNewFile *.wsb       set filetype=xml")
vim.cmd("au BufRead,BufNewFile *.vbsvrconf set filetype=json")
vim.cmd("au BufRead,BufNewFile *.vbsvrkb   set filetype=json")

-- Folding methods
vim.wo.foldmethod      = 'expr'
vim.wo.foldexpr        = 'nvim_treesitter#foldexpr()'
vim.opt.foldlevelstart = 99

-- => Neovide
vim.cmd([[if exists('g:neovide')
		let g:neovide_cursor_animation_length=0.1
		let g:neovide_cursor_trail_length=0.5
		let g:neovide_cursor_vfx_mode = ''

		command -nargs=0 NeovideToggleFullscreen :let g:neovide_fullscreen = !g:neovide_fullscreen
		nnoremap <a-cr> :NeovideToggleFullscreen<cr>
	endif]])
-- cmd([[au CursorHold,CursorHoldI * lua require('config.code-action-utils').code_action_listener()]])
